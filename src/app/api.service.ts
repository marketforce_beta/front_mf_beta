import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //private localUrl = 'http://localhost:3000/api';
  private localUrl = 'http://185.28.23.230:3000/api';
  constructor(private http: HttpClient) { }

  //LOGIN
  login(user: any): Observable<any>{
    return this.http.post(this.localUrl+"/login", user);
  }
  login2(){
    return this.http.get(this.localUrl+"/suser");
  }

  //CREATE COMPANY
  createComapny(company: any): Observable<any>{
    /*let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Content-Type', 'multipart/form-data');
    return this.http.post(this.localUrl+"/company", company, {"headers":headers})*/
    return this.http.post(this.localUrl+"/company", company)
  }
  //CREATE ACOUNT SUPER USER
  createAcount(user: any, db: any): Observable<any>{
    return this.http.post(this.localUrl+"/user/"+db, user);
  }
}
