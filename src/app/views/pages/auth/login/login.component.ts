import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private _success = new Subject<string>();

  returnUrl: any;
  name: string;
  password: string;
  token: string;
  msg : string;
  datoUsuario;
  intro:boolean=true;

  @ViewChild('loginAlert', {static: false}) loginAlert: NgbAlert;


  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) {
    this.name = ""
    this.password = ""
    this.msg = "";

    setTimeout(() => {
      this.intro = false;
    }, 4100);
  }


  ngOnInit(): void {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    //this.returnUrl = this.api.getSuperUser();

      this._success.subscribe(message => this.msg = message);
      this._success.pipe(debounceTime(5000)).subscribe(() => {
        if (this.loginAlert) {
          this.loginAlert.close();
        }

      });
  }

  onLoggedin(e) {
    e.preventDefault();
    localStorage.setItem('isLoggedin', 'true');
    const user = {username: this.name, password: this.password}
    this.api.login(user)

        .subscribe(
          response => {

            this.token = response['token']
            if(this.token != ""){
              localStorage.setItem('usuario', JSON.stringify(response))

              //recuperar dato
              //this.datoUsuario = JSON.parse(localStorage.getItem('usuario'));
              this.router.navigate([this.returnUrl])
            }else{
              this.msg = "Wrong Credentials";
            }

          },
          err => {
            if (err.statusText == "Unknown Error") {
              this.msg = "connection error"

            } else {
              this.msg = err.error['message']
              setTimeout(() => this.loginAlert.close(), 5000);

            }
          }, () => {
            //this.router.navigate([this.returnUrl]);
            console.log("Completed");
          }


        );

  }

}
