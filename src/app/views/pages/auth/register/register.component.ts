import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  dangerAlert;
  firstName;
  lastName;
  userName;
  email;
  password;
  nameCompany;
  abbreviation;
  address;
  country;
  city;
  state;
  zip_code;
  emailFrom;
  phone;
  database;
  webSite;
  file_logo;
  file_order;
  msg;
  company_id;

  returnUrl: any;


  validationForm1: FormGroup;
  validationForm2: FormGroup;

  isForm1Submitted: Boolean;
  isForm2Submitted: Boolean;

  @ViewChild('wizardForm') wizardForm: BaseWizardComponent;


  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private api: ApiService,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';

    /**
     * form1 value validation
     */
     this.validationForm1 = this.formBuilder.group({
      firstName : ['', Validators.required],
      lastName : ['', Validators.required],
      userName : ['', Validators.required],
      email : ['', [Validators.required, Validators.email]],
      password : ['', [Validators.required, Validators.minLength(8)]]
    });

    /**
     * formw value validation
     */
    this.validationForm2 = this.formBuilder.group({
      nameCompany : ['', Validators.required],
      abbreviation : ['', Validators.required],
      emailFrom : ['', [Validators.required, Validators.email]],
      address : ['', Validators.required],
      country : ['', Validators.required],
      city : ['', Validators.required],
      state : ['', Validators.required],
      zip_code : ['', Validators.required],
      phone : ['', Validators.required],
      database : ['', Validators.required],
      webSite : ['', Validators.required],
      file_logo : ['', [
          RxwebValidators.extension({extensions:["jpeg","png","jpg"]})
        ],

      ],
      file_order:['', [
        RxwebValidators.extension({extensions:["jpeg","png","jpg"]})
      ],

    ],
    });

    this.isForm1Submitted = false;
    this.isForm2Submitted = false;
  }

  openFileBrowser(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.querySelector("#file_logo") as HTMLElement;
    element.click()
  }

  handleFileInput(event: any) {
    if (event.target.files.length) {
      let element: HTMLElement = document.querySelector("#file_logo + .input-group .file-upload-info") as HTMLElement;
      let fileName = event.target.files[0].name;
      this.file_logo = event.target.files[0]
      element.setAttribute( 'value', fileName)
    }
  }

  openFileBrowserOrder(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.querySelector("#file_order") as HTMLElement;
    element.click()
  }

  uploadOrderLogo(event: any) {
    if (event.target.files.length) {
      let element: HTMLElement = document.querySelector("#file_order + .input-group .file-upload-info") as HTMLElement;
      let fileName = event.target.files[0].name;
      this.file_order = event.target.files[0]
      element.setAttribute( 'value', fileName)
    }
  }

  /**
   * Wizard finish function
   */
   finishFunction(event: any) {

    // Create form data
    const formData = new FormData();
    console.log(this.file_logo.name)

    // Store form name as "file" with file data
    formData.append("logo", this.file_logo, this.file_logo.name);
    formData.append("orders_logo", this.file_order, this.file_order.name);
    formData.append("full_name", this.nameCompany);
    formData.append("address", this.address);
    formData.append("abbreviation", this.abbreviation);
    formData.append("country", this.country);
    formData.append("city", this.city);
    formData.append("state", this.state);
    formData.append("zip_code", this.zip_code);
    formData.append("email_from", this.emailFrom);
    formData.append("phone", this.phone);
    formData.append("database", this.database);
    formData.append("website", this.webSite);
    formData.append("name", this.firstName);
    formData.append("last_name", this.lastName);
    formData.append("password", this.password);
    formData.append("username", this.userName);
    formData.append("email", this.email);

    console.log(formData)

    console.log(formData.getAll('full_name'))


    this.api.createComapny(formData)

        .subscribe(
          response => {

            if(response['status'] == "success"){
              this.router.navigate([this.returnUrl])

            }else{
              this.msg = "Error Creating Account";
              console.log(this.msg);
            }

          },
          err => {
            if (err.statusText == "Unknown Error") {
              this.msg = "connection error"
              console.log(this.msg);

            } else {
              this.msg = err.error['message']
              console.log(this.msg);
              setTimeout(() => this.dangerAlert.close(), 5000);

            }
          }, () => {
            //this.router.navigate([this.returnUrl]);
            console.log("Completed");
          }


        );


    alert('Successfully Completed');
  }

  /**
   * Returns form
   */
   get form1() {

    return this.validationForm1.controls;
  }

  /**
   * Returns form
   */
  get form2() {
    return this.validationForm2.controls;
  }

  /**
   * Go to next step while form value is valid
   */
  form1Submit() {
    if(this.validationForm1.valid) {
      this.wizardForm.goToNextStep();
    }
    this.isForm1Submitted = true;
  }

  /**
   * Go to next step while form value is valid
   */
  form2Submit() {
    if(this.validationForm2.valid) {
      this.wizardForm.goToNextStep();
    }
    this.isForm2Submitted = true;
  }

  onRegister(e) {
    e.preventDefault();
    localStorage.setItem('isLoggedin', 'true');
    if (localStorage.getItem('isLoggedin')) {
      this.router.navigate(['/login']);
    }
  }

}
