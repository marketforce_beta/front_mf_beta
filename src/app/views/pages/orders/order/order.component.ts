import { Component, OnInit } from '@angular/core';
import { DataTable } from "simple-datatables";
import { ORDERS } from './mock-orders';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {
  orderList = ORDERS;
  data = {
    headings:['Invoice Number', 'Date', 'User', 'Order Info', 'Status', 'Notes', 'Actions'],
    data:this.orderList
  }
  ngOnInit(): void {
    const dataTable = new DataTable("#dataTableOrders",{
      data:this.data
    });
  }

}
