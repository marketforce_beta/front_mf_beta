
//Test interface
export interface Order {
  invoice:number,
  date:string,
  user:string,
  info:string,
  status:string,
  notes:string
}
