import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
// Ng-select
import { NgSelectModule } from '@ng-select/ng-select';

import { OrdersComponent } from './orders.component';
import { OrderComponent } from './order/order.component';
import { CreateOrderComponent } from './create-order/create-order.component';
import { PrintPDFOrderComponent } from './print-pdforder/print-pdforder.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [
      {
        path: '',
        redirectTo: 'order',
        pathMatch: 'full'
      },
      {
        path: 'order',
        component: OrderComponent
      },
      {
        path: 'order/create',
        component: CreateOrderComponent
      },
      {
        path: 'order/print_pdf',
        component: PrintPDFOrderComponent
      },

    ]
  }
]

@NgModule({
  declarations: [OrdersComponent, OrderComponent, CreateOrderComponent, PrintPDFOrderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    FormsModule,
    NgSelectModule
  ]
})
export class OrdersModule { }
