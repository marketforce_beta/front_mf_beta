import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPDFOrderComponent } from './print-pdforder.component';

describe('PrintPDFOrderComponent', () => {
  let component: PrintPDFOrderComponent;
  let fixture: ComponentFixture<PrintPDFOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintPDFOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPDFOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
