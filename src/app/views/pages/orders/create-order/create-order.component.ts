import { Component, OnInit } from '@angular/core';
import { dataOrder } from './order-data';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

//service
import { CompanyService } from 'src/app/services/company.service';
import { $ } from 'protractor';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})


export class CreateOrderComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private companyService: CompanyService
    ) { }
  dataOrder:dataOrder={
    firstName:'',
    lastName:'',
    address:'',
    city:'',
    state:'',
    zipCode:'',
    phone:'',
    email:'',
    description:'',
    width:0,
    roofL:0,
    frame:0,
    leg:0,
    gauge:'',
    price:0,
    topColor:'',
    sideColor:'',
    endsColor:'',
    trimColor:'',
    roof:'',
    legLength:0,
    options:{
      aditional:'',
      aditionalQty:0,
      aditionalEach:0,
      aditionalTotal:0,
      side:'',
      sideQty:0,
      sideEach:0,
      sideTotal:0,
      ends:'',
      endsQty:0,
      endsEach:0,
      endsTotal:0,
      gable:'',
      gableQty:0,
      gableEach:0,
      gableTotal:0,
      rollUp:'',
      rollColor:'',
      rollQty:0,
      rollEach:0,
      rollTotal:0,
      wall:'',
      wallQty:0,
      wallEach:0,
      wallTotal:0,
      window:'',
      windowQty:0,
      windowEach:0,
      windowTotal:0,
    },
    specialInstructions:'',
    landLeven:'',
    readyInstallation:'',
    electricity:'',
    surface:'',
    payment:'',
    totalSale:0,
    subtotal:0,
    tax:0,
    noTax:0,
    deposit:0,
    balance:0,
  };
  states:string[]=['Alabama', 'Alaskza', 'Arizona', 'Arkanzas', 'California','Colorado'];
  descriptions:string[]=['Agricultural', 'Carpots', 'Garages', 'RV Covers', 'Sheds', 'Truss'];
  gauges:any[]=[{value:'Standard', text:'14 (Standard)'}, {value:'Optional', text:'12 (Optional)'}];
  colors:any[]=[
    {value:'white', color:'White'},
    {value:'forest_green',text:'Forest Green'},
    {value:'hawaiian_blue', text:'Hawaiian Blue'},
    {value:'zinc_gray', text: 'Zinc Gray'},
    {value:'barn_red', text:'Barn Red'},
    {value:'light_stone', text:'Light Stone'},
    {value:'pebble_beige', text:'Pebble Beige'},
    {value:'pewter_gray', text:'Pewter Gray'},
    {value:'brown', text:'Brown'},
    {value:'mocha_tan', text:'Mocha Tan'},
    {value:'taupe', text:'Taupe'},
    {value:'burgundy', text:'Burgundy'},
    {value:'cardinal_red', text:'Cardinal Red'},
    {value:'galvalume', text:'Galvalume'}
  ];
  roofs:any[]=[
    {value:'regular_frame', text:'Regular Frame'},
    {value:'a-frame', text:'A Frame'},
    {value:'vertical_roof', text:'Vertical Roof'},
    {value:'all_vertical', text:'All Vertical'}
  ];
  rollups:any[]=[
    {text:"6'x6'",value:"6x6"},
    {text:"8'x7'",value:"8x7"},
    {text:"9'x7'",value:"9x7"},
    {text:"10'x8'",value:"10x8"},
    {text:"10'x10'",value:"10x10"},
    {text:"10'x12'",value:"10x12"},
    {text:"12'x12'",value:"12x12"},
    {text:"12'x14'",value:"12x14"}
  ];
  walls:any[]=[
    {value:'32x72',text:'32" X 72"'},
    {value:'36x80', text:'36" X 80"'},
  ];
  windows:any[]=[
    {value:'24" X 36"',text:'24X36'},
    {value:'30" X 36"',text:'30X36'}
  ];
  dealers:any[]=[
    {id:1,name:'Gustavo De La Rosa',address:'Col. El Campanario',phone:'5512512536'},
    {id:2,name:'Fabian Rivera',address:'Col. Centro',phone:'5512598536'},
    {id:3,name:'Juan Francisto Gil',address:'Col. Centro Sur',phone:'5512533236'},
  ];
  dealer:any={};
  notes:string='';
  currentEnterprise:any;
  clients:any[]=[
    {url:'./assets/images/american.png',name:'All American', address:'3007 NW Stallings Dr Nacogdoches , Texas 75964', phone:'1(833) 319-0301'},
    {url:'./assets/images/castle.png',name:'Castle Metal', address:'915 WILSHIRE BLVD #1600, LOS ANGELES, CA 90017-6410, USA', phone:'1(833) 785-0301'},
    {url:'./assets/images/united.png',name:'United Metal Structure', address:'Baltimore, Maryland, Estados Unidos', phone:'+1-410-955-5000'},
    {url:'./assets/images/roofing.png',name:'Del Valle Metal Roofing', address:'55 Fruit St, Boston, MA 02114, Estados Unidos', phone:'+1 617-726-2000'},
    {url:'./assets/images/steel.png',name:'All Steel Carpots', address:'251 E Huron St, Chicago, IL 60611, Estados Unidos', phone:'+1 312-926-2000'},
    {url:'./assets/images/american.png',name:'East Texas Carpots', address:'2025 E Street, NW Washington, DC 20006', phone:'1-800-RED CROSS (1-800-733-2767)'}
  ];
  customNotes:any[]=[];
  date = new Date();
  today = this.date.getFullYear()+'-'+(this.date.getMonth()+1)+'-'+this.date.getDate();
  stateAdditional = true;
  extraItems:any=[];
  totalExtraItems:number=0;

  ngOnInit(): void {
    // array of objects
  }

  openModal(content){
    this.modalService.open('',{centered: true}).result.then((result) =>{
      console.log(result)
    })
  }

  saveNotes(){
    this.customNotes.push({dealer:this.dealer.name,note:this.notes, date:this.today});
    this.dealer = {};
    this.notes = '';
  }

  //Calculations
  calculateTotal(option){
    switch (option) {
      case 'add':
        this.dataOrder.options.aditionalTotal = this.dataOrder.options.aditionalQty * this.dataOrder.options.aditionalEach;
        break;
      case 'side':
          this.dataOrder.options.sideTotal = this.dataOrder.options.sideQty * this.dataOrder.options.sideEach;
          break;
      case 'ends':
          this.dataOrder.options.endsTotal = this.dataOrder.options.endsQty * this.dataOrder.options.endsEach;
          break;
      case 'gab':
            this.dataOrder.options.gableTotal = this.dataOrder.options.gableQty * this.dataOrder.options.gableEach;
            break;
      case 'roll':
            this.dataOrder.options.rollTotal = this.dataOrder.options.rollQty * this.dataOrder.options.rollEach;
            break;
      case 'window':
            this.dataOrder.options.windowTotal = this.dataOrder.options.windowQty * this.dataOrder.options.windowEach;
            break;
      case 'wall':
            this.dataOrder.options.windowTotal = this.dataOrder.options.windowQty * this.dataOrder.options.windowEach;
            break;
    }
    this.calculateTotalSale()
  }


  validationLeg(){
    if(this.dataOrder.legLength > 6){
      this.dataOrder.options.aditionalQty = this.dataOrder.legLength - 6;
      this.dataOrder.options.aditional = 'aditional';
    }else{
      this.dataOrder.options.aditionalQty = 0;
    }
  }

  calculateTax(){
    let percent= (this.dataOrder.tax * this.dataOrder.totalSale)/100;
    this.dataOrder.subtotal = this.dataOrder.totalSale + percent;
  }

  calculateNoTax(){
    if(this.dataOrder.noTax != 0){
      this.dataOrder.tax = 0;
      this.calculateTax();
    }else{
      this.calculateTax();
    }
  }
  calculateDeposit(){
    if(this.dataOrder.deposit < this.dataOrder.subtotal){
      this.dataOrder.balance = this.dataOrder.subtotal - this.dataOrder.deposit;
    }
  }

  calculateTotalSale(){
    this.dataOrder.totalSale = this.totalExtraItems + this.dataOrder.price + this.dataOrder.options.aditionalTotal + this.dataOrder.options.sideTotal +this.dataOrder.options.endsTotal + this.dataOrder.options.gableTotal + this.dataOrder.options.rollTotal + this.dataOrder.options.windowTotal;
  }

  addExtraItem(){
    this.extraItems.push({item:'',qty:0,price:0});
  }
  popExtraItem(index){
    this.extraItems.splice(index,1);
    this.calculateExtraItems()
  }

  calculateExtraItems(){
    let total = 0;
    this.extraItems.forEach(item => {
      total = total + (item.qty * item.price);
    });

    this.totalExtraItems =  total;
    this.calculateTotalSale();
  }

  getLogo(){
    let company = this.companyService.getCurrentCompany();
    let logo = this.companyService.getImgCompany(company);
    return logo;
  }

  saveOrder(content){
    this. extraItems=[];
    this.modalService.open(content,{centered: true}).result.then((result) =>{
      console.log(result)
    })
    this.dataOrder ={
      firstName:'',
      lastName:'',
      address:'',
      city:'',
      state:'',
      zipCode:'',
      phone:'',
      email:'',
      description:'',
      width:0,
      roofL:0,
      frame:0,
      leg:0,
      gauge:'',
      price:0,
      topColor:'',
      sideColor:'',
      endsColor:'',
      trimColor:'',
      roof:'',
      legLength:0,
      options:{
        aditional:'',
        aditionalQty:0,
        aditionalEach:0,
        aditionalTotal:0,
        side:'',
        sideQty:0,
        sideEach:0,
        sideTotal:0,
        ends:'',
        endsQty:0,
        endsEach:0,
        endsTotal:0,
        gable:'',
        gableQty:0,
        gableEach:0,
        gableTotal:0,
        rollUp:'',
        rollColor:'',
        rollQty:0,
        rollEach:0,
        rollTotal:0,
        wall:'',
        wallQty:0,
        wallEach:0,
        wallTotal:0,
        window:'',
        windowQty:0,
        windowEach:0,
        windowTotal:0,
      },
      specialInstructions:'',
      landLeven:'',
      readyInstallation:'',
      electricity:'',
      surface:'',
      payment:'',
      totalSale:0,
      subtotal:0,
      tax:0,
      noTax:0,
      deposit:0,
      balance:0,
    };
  }
}
