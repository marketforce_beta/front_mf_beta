import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CarouselModule } from 'ngx-owl-carousel-o';

import { TutorialsComponent } from './tutorials.component';
import { TutorialComponent } from './tutorial/tutorial.component';


const  routes : Routes = [
  {
    path: '',
    component: TutorialsComponent,
    children: [
      {
        path: '',
        redirectTo: 'tutorial',
        pathMatch: 'full'
      },
      {
        path: 'tutorial',
        component: TutorialComponent,
      }
    ]
  }
]

@NgModule({
  declarations: [TutorialsComponent, TutorialComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CarouselModule,

  ]
})
export class TutorialsModule { }
