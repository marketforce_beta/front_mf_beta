import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  autoPlayExampleOptions: OwlOptions = {
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive:{
      0:{
          items:2
      },
      600:{
          items:3
      },
      1000:{
          items:4
      }
    }
  }

  slidesStore = [
    {
      id:'1',
      src:'https://www.youtube.com/embed/RrVWSqe5t8E',
      alt:'Video_1',
      title:'Video_1'
    },
    {
      id:'2',
      src:'https://www.youtube.com/embed/RrVWSqe5t8E',
      alt:'Video_2',
      title:'Video_2'
    },
    {
      id:'3',
      src:'https://www.youtube.com/embed/RrVWSqe5t8E',
      alt:'Video_3',
      title:'Video_3'
    },
    {
      id:'4',
      src:'https://www.youtube.com/embed/RrVWSqe5t8E',
      alt:'Video_4',
      title:'Video_4'
    },

  ]

  constructor() { }

  ngOnInit(): void {
  }

}
