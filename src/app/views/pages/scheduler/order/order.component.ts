import { Component, OnInit } from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  title = 'google-maps';

  constructor() {

  }

  ngOnInit(): void {
    const loader = new Loader({
      apiKey: "AIzaSyAKXhW-9e4a_0Qxdrbt-ddt84CjbOMo15Q",
      version: "weekly",
      libraries: ["places"]
    })

    const mainPosition = { lat: 35.722983, lng: -95.402559 };
    const position1 = { lat: 35.740979, lng: -95.418772 };
    const position2 = { lat: 35.740399, lng: -95.394173 };
    const position3 = { lat: 35.733025, lng: -95.405196 };
    const position4 = { lat: 35.710651, lng: -95.405298 };

    const secretMessages = [
      "<h4># 100</h4> <br> Customer: Fabian Rivera <br> Created: 28/01/2022 <br> Installed: 28/02/2022 <br> Price: $25000",
      "# 101 <br> Customer: Victor Rojas <br> Created: 28/01/2022 <br> Installed: 28/02/2022 <br> Price: $21000",
      "# 102 <br> Customer: Gustavo De la Rosa <br> Created: 28/01/2022 <br> Installed: 28/02/2022 <br> Price: $22000",
      "# 103 <br> Customer: PAblo Ortiz <br> Created: 28/01/2022 <br> Installed: 28/02/2022 <br> Price: $23000",
    ];



    loader
    .load()
    .then((google) => {
      const map =new google.maps.Map(document.getElementById("map"), {
        center: mainPosition,
        zoom: 12
      });

      const marker0 = new google.maps.Marker({
        position: mainPosition,
        map,
        title: "Legacy"
      });

      const marker1 = new google.maps.Marker({
        position: position1,
        map,
        title: "100"
      });


      attachSecretMessage(marker1, secretMessages[0]);


      const marker2 = new google.maps.Marker({
        position: position2,
        map,
        title: "101"
      });

      attachSecretMessage(marker2, secretMessages[1]);

      const marker3 = new google.maps.Marker({
        position: position3,
        map,
        title: "102"
      });

      attachSecretMessage(marker3, secretMessages[2]);

      const marker4 = new google.maps.Marker({
        position: position4,
        map,
        title: "103"
      });

      attachSecretMessage(marker4, secretMessages[3]);

      // Definiendo las coordenadas para el path del polígono
      /*
      var triangleCoords = [
        { lat: 35.740979, lng: -95.418772 },
        { lat: 35.740399, lng: -95.394173 },

        { lat: 35.733025, lng: -95.405196 },
        { lat: 35.710651, lng: -95.405298 }
      ];
      */

      // Construyendo el póligono
      /*
      var poligono = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
      });

      poligono.setMap(map);
      */


    })

    function attachSecretMessage(marker: google.maps.Marker, secretMessage: string) {
      const infowindow = new google.maps.InfoWindow({
        content: secretMessage
      });

      marker.addListener("click", () => {
        infowindow.open(marker.get("map"), marker);
      });
    }


    // The marker, positioned at Uluru
    /*new google.maps.Marker({
      position: mainPosition,
      map,
      title: "Legacy"
    }); */
  }

}
