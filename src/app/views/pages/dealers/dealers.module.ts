import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
// Ng-select
import { NgSelectModule } from '@ng-select/ng-select';

import { DealersComponent } from './dealers.component';
import { DealerComponent } from './dealer/dealer.component';
import { CreateDealerComponent } from './create-dealer/create-dealer.component';



const routes: Routes = [
  {
    path: '',
    component: DealersComponent,
    children: [
      {
        path: '',
        redirectTo: 'dealer',
        pathMatch: 'full'
      },
      {
        path: 'dealer',
        component: DealerComponent
      },


    ]
  }
]

@NgModule({
  declarations: [DealersComponent, DealerComponent, CreateDealerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    FormsModule,
    NgSelectModule
  ]
})
export class DealersModule { }
