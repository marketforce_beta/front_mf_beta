import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal,NgbDateStruct  } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-create-dealer',
  templateUrl: './create-dealer.component.html',
  styleUrls: ['./create-dealer.component.scss']
})
export class CreateDealerComponent implements OnInit {

  formCreateDealer;
  selectedDate: NgbDateStruct;

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  openModalCreateDealer(content) {

    this.modalService.open(content, {size: 'xl'}).result.then((result) => {
      console.log("Modal closed" + result);
    }).catch((res) => {});
  }

}
