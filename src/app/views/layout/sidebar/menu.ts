import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
  /*
  {
    label: 'Main',
    isTitle: true
  },
  */
  {
    label: 'Dashboard',
    icon: 'home',
    link: 'dashboard'
  },
  {
    label: 'Dealer Forms',
    icon: 'mail',
    //link: '/dashboard'
  },
  {
    label: 'Dealers',
    icon: 'truck',
    subItems: [
      {
        label: 'Dealer',
        link: '/dashboard/dealers/dealer'
      },
      {
        label: 'Dealer Lookup',
        //link: '/dashboard'
      },
      {
        label: 'Dealer Map',
        //link: '/dashboard'
      },
    ]
  },
  {
    label: 'Customers',
    icon: 'smile',
    subItems: [
      {
        label: 'Customer',
        link: '/dashboard/customers/customer'
      },
    ]
  },
  {
    label: 'Inventory',
    icon: 'anchor',
    //link: '/dashboard'
    subItems: [
      {
        label: 'Items'
      }
    ]
  },
  {
    label: 'New Dealer',
    icon: 'file-text',
    //link: '/dashboard'
  },
  {
    label: 'Orders',
    icon: 'clipboard',
    link: '/dashboard/orders/order'
  },
  {
    label: 'Repairs',
    icon: 'layout',
    //link: '/dashboard'
  },
  {
    label: 'Reports',
    icon: 'smile',
    //link: '/dashboard'
  },
  {
    label: 'Repos',
    icon: 'smile',
    //link: '/dashboard'
  },
  {
    label: 'Schedulers',
    icon: 'map',
    subItems: [
      {
        label: 'Orders',
        link: '/dashboard/schedulers/order'
      },
      {
        label: 'Dealers',
        link: '/dashboard/schedulers/dealer'
      },
    ]
    //link: '/dashboard'
  },
  {
    label: 'Visit Notes',
    icon: 'smile',
    //link: '/dashboard'
  },
  {
    label: 'Room',
    icon: 'smile',
    //link: '/dashboard'
  },
  {
    label: 'View Open Tickets',
    icon: 'smile',
    //link: '/dashboard'
  },
  {
    label: 'Tutorial',
    icon: 'youtube',
    link: '/dashboard/tutorials/tutorial'
  },

];
